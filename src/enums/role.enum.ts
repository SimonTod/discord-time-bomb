export enum Role {
  Sherlock = 'Sherlock',
  Moriarty = 'Moriarty',
}

export function GetRolesToAttribute(numberOfPlayers: number): Array<Role> {
  const roles: Array<Role> = [];

  let moriarties: number;
  let sherlocks: number;
  if (numberOfPlayers === 4 || numberOfPlayers == 5) {
    moriarties = 2;
    sherlocks = 3;
  } else if (numberOfPlayers == 6) {
    moriarties = 2;
    sherlocks = 4;
  } else {
    moriarties = 3;
    sherlocks = 5;
  }

  for (let i = 0; i < moriarties; i++) {
    roles.push(Role.Moriarty);
  }
  for (let i = 0; i < sherlocks; i++) {
    roles.push(Role.Sherlock);
  }

  return roles;
}
