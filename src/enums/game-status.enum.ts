export enum GameStatus {
  WaitingToStart,
  WaitingForPlayers,
  Started,
  Finished,
}
