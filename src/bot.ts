import {
  Client,
  Message,
  TextChannel,
  DMChannel,
  NewsChannel,
  Snowflake,
} from 'discord.js';
import { Game } from './game';
import { GameStatus } from './enums/game-status.enum';

export class Bot {
  private client: Client;
  private game: Game;

  constructor(client: Client) {
    this.client = client;
    this.game = new Game(this.client);
  }

  public listen(): Promise<string> {
    this.client.on('message', (message: Message) => {
      if (message.author.bot) {
        console.log('Ignoring bot message!');
        return;
      }

      console.log('Message received! Contents: ', message.content);

      if (
        message.content == '!play' &&
        this.game.status == GameStatus.WaitingToStart
      ) {
        console.log('Starting game! Launched by: ', message.author.username);
        message.reply('Lauching your game!');
        this.game.startGame(message.channel, message.author);
      } else if (
        message.content == '!play' &&
        this.game.status == GameStatus.Finished
      ) {
        console.log('Starting game! Launched by: ', message.author.username);
        message.reply('Lauching your game!');
        this.game = new Game(this.client);
        this.game.startGame(message.channel, message.author);
      } else if (message.content == '!play') {
        message.reply('A game is already running');
      } else if (message.content == '!clean') {
        Bot.cleanChannel(message.channel, this.game);
      }
    });

    return this.client.login(process.env.TOKEN);
  }

  private static cleanChannel(
    channel: TextChannel | DMChannel | NewsChannel,
    runningGame: Game,
  ): void {
    const messagesToBeDeleted: Array<string> = ['!play', '!clean'];
    const messageIdsNotToDelete: Array<Snowflake> = [];
    if (runningGame.message) {
      messageIdsNotToDelete.push(runningGame.message.id);
    }
    runningGame.users.forEach(user => {
      if (user.message) {
        messageIdsNotToDelete.push(user.message.id);
      }
    });

    channel.messages.fetch().then(messages => {
      messages
        .filter(
          m =>
            !messageIdsNotToDelete.includes(m.id) &&
            (m.author.bot ||
              (messagesToBeDeleted.includes(m.content) &&
                m.channel.type == 'text')),
        )
        .forEach(message => {
          message.delete();
        });
    });
  }
}
