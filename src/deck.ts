import mergeImg from 'merge-img';
import Jimp from 'jimp';
import { Card } from './enums/card.enum';
import * as path from 'path';

export class Deck {
  public cards: Array<Card>;

  constructor(numberOfPlayers: number) {
    this.cards = [Card.Bomb];

    for (let i = 0; i < numberOfPlayers; i++) {
      this.cards.push(Card.GoodWire);
    }

    for (let i = numberOfPlayers + 1; i < numberOfPlayers * 5; i++) {
      this.cards.push(Card.WrongWire);
    }
  }

  public getCardsForUser(numberOfCards: number): Array<Card> {
    const cardsForPlayer: Array<Card> = [];
    for (let i = 0; i < numberOfCards; i++) {
      const random = Math.floor(Math.random() * this.cards.length);
      cardsForPlayer.push(this.cards[random]);
      this.cards.splice(random, 1);
    }
    return cardsForPlayer;
  }

  public static getCardImage(card: Card): string {
    let cardImage = '';
    switch (card) {
      case Card.Bomb:
        cardImage = path.join(__dirname, '/assets/') + 'bomb.png';
        break;
      case Card.GoodWire:
        cardImage = path.join(__dirname, '/assets/') + 'good_wire.png';
        break;
      case Card.WrongWire:
        cardImage = path.join(__dirname, '/assets/') + 'wrong_wire.png';
        break;
      default:
        break;
    }
    return cardImage;
  }

  private static getCardImages(cards: Array<Card>): Promise<Array<string>> {
    const cardImages: Array<string> = [];
    cards.forEach(card => {
      cardImages.push(Deck.getCardImage(card));
    });

    return Promise.resolve(cardImages);
  }

  public static getCardsImage(cards: Array<Card>): Promise<Jimp | string> {
    if (cards.length > 0) {
      return this.getCardImages(cards).then((cardImages: Array<string>) => {
        return mergeImg(cardImages).then(img => {
          return Promise.resolve(img);
        });
      });
    } else {
      return Promise.resolve('You have no cards left.');
    }
  }
}
