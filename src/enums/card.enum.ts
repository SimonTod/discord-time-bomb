export enum Card {
  Bomb = '💣',
  GoodWire = '✅',
  WrongWire = '❎',
}
