import {
  User as DiscordUser,
  PartialUser,
  Message,
  MessageEmbed,
} from 'discord.js';
import randomstring from 'randomstring';
import { Role } from './enums/role.enum';
import { Card } from './enums/card.enum';
import { Deck } from './deck';
import * as fs from 'fs';
import * as path from 'path';
import Jimp from 'jimp/*';

export class User {
  public static avatars: Array<string> = [
    '😀',
    '🤣',
    '😇',
    '🤓',
    '😎',
    '😤',
    '🤮',
    '🤠',
  ];

  public static cardChoiceEmoji: Array<string> = ['1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣'];

  public discordUser: DiscordUser | PartialUser;
  public role: Role | null;
  public cards: Array<Card>;
  public message: Message | null;

  constructor(user: DiscordUser | PartialUser) {
    this.discordUser = user;
    this.role = null;
    this.cards = [];
    this.message = null;
  }

  public sendRoleAndCards(shouldResendCards = true): void {
    Deck.getCardsImage(this.cards).then((cardsImage: Jimp | string) => {
      if (typeof cardsImage == 'string') {
        this.sendMessage(shouldResendCards);
      } else {
        this.getRandomTmpFilePath().then(filePath => {
          cardsImage.write(filePath, () => {
            this.sendMessage(shouldResendCards, filePath);
          });
        });
      }
    });
  }

  private sendMessage(shouldResendCards = true, imagePath?: string): void {
    const messageEmbedCards = new MessageEmbed();

    if (imagePath) {
      messageEmbedCards
        .setDescription('Here are your cards')
        .attachFiles([imagePath]);
    } else {
      messageEmbedCards.setDescription('You have no cards left');
    }

    if (!this.message) {
      const messageEmbedRole = new MessageEmbed()
        .setTitle('Your role is: ' + this.role)
        .attachFiles([User.getRandomRoleImage(this.role)]);
      if (this.role == Role.Sherlock) {
        messageEmbedRole
          .setColor('#0b7489')
          .setDescription('Your goal is to ...');
      } else if (this.role == Role.Moriarty) {
        messageEmbedRole
          .setColor('#ac203a')
          .setDescription('Your goal is to...');
      }
      this.discordUser.send(messageEmbedRole).then(() => {
        this.discordUser.send(messageEmbedCards).then((message: Message) => {
          this.message = message;
          if (imagePath) {
            this.deleteTmpFile(imagePath);
          }
        });
      });
    } else {
      if (shouldResendCards) {
        this.message.delete().then(() => {
          this.discordUser.send(messageEmbedCards).then((message: Message) => {
            this.message = message;
            if (imagePath) {
              this.deleteTmpFile(imagePath);
            }
          });
        });
      }
    }
  }

  public removeCard(): Card {
    const random = Math.floor(Math.random() * this.cards.length);
    const cardToRemove = this.cards[random];
    this.cards.splice(random, 1);
    return cardToRemove;
  }

  public static getUserAvatar(indexOfUser: number): string {
    return this.avatars[indexOfUser];
  }

  public static getCardChoiceEmoji(indexOfCard: number): string {
    return this.cardChoiceEmoji[indexOfCard];
  }

  private static getRandomRoleImage(role: Role | null): string {
    const roleFiles: Array<string> = [];
    const regex =
      role == Role.Moriarty
        ? /role_moriarty_\d\.png/g
        : /role_sherlock_\d\.png/g;

    const files = fs.readdirSync(path.join(__dirname, '/assets/'));
    files.forEach(file => {
      if (file.match(regex)) {
        roleFiles.push(file);
      }
    });
    const random = Math.floor(Math.random() * roleFiles.length);
    return path.join(__dirname, '/assets/') + roleFiles[random];
  }

  private getRandomTmpFilePath(): Promise<string> {
    if (process.env.TMP_FOLDER) {
      const tmpFoler: string = process.env.TMP_FOLDER;
      if (!fs.existsSync(tmpFoler)) {
        return fs.promises.mkdir(tmpFoler).then(() => {
          return Promise.resolve(tmpFoler + randomstring.generate() + '.png');
        });
      } else {
        return Promise.resolve(tmpFoler + randomstring.generate() + '.png');
      }
    } else return Promise.reject('TMP_FOLDER env var does not exist');
  }

  private deleteTmpFile(filePath: string): void {
    fs.unlinkSync(filePath);
  }

  public static deleteTmpFiles(): void {
    if (process.env.TMP_FOLDER) {
      const tmpFoler: string = process.env.TMP_FOLDER;
      fs.promises.readdir(tmpFoler).then((files: Array<string>) => {
        files.forEach(file => {
          fs.unlinkSync(tmpFoler + file);
        });
      });
    }
  }
}
