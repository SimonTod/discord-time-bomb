import {
  Client,
  TextChannel,
  DMChannel,
  NewsChannel,
  Message,
  MessageReaction,
  User as DiscordUser,
  PartialUser,
  MessageEmbed,
} from 'discord.js';
import { GameStatus } from './enums/game-status.enum';
import { Deck } from './deck';
import { User } from './user';
import { Role, GetRolesToAttribute } from './enums/role.enum';
import { Card } from './enums/card.enum';

export class Game {
  public status: GameStatus;
  public channel: TextChannel | DMChannel | NewsChannel | null;
  public message: Message | null;
  public users: Array<User>;
  private client: Client;
  private initiator: DiscordUser | null;
  private deck: Deck | null;
  private playingUserIndex: number;
  private chosenUserIndex: number;
  private lastPickedCard: Card | null;
  private roundsToPlay: number;
  private cardsToPick: number;

  private _minPlayers = 4;
  private _maxPlayers = 8;

  constructor(client: Client) {
    this.status = GameStatus.WaitingToStart;
    this.channel = null;
    this.message = null;
    this.users = [];
    this.client = client;
    this.initiator = null;
    this.deck = null;
    this.playingUserIndex = -1;
    this.chosenUserIndex = -1;
    this.lastPickedCard = null;
    this.roundsToPlay = 4;
    this.cardsToPick = 0;
  }

  public startGame(
    channel: TextChannel | DMChannel | NewsChannel,
    initiator: DiscordUser,
  ): void {
    this.status = GameStatus.WaitingForPlayers;
    this.channel = channel;
    this.initiator = initiator;
    this.setStartGameMessage().then((message: Message) => {
      this.message = message;
      this.message.react('🖐️').then((reaction: MessageReaction) => {
        this.client.on('messageReactionAdd', (reaction, user) =>
          this.onMessageReactionAdd(reaction, user),
        );
        this.client.on('messageReactionRemove', (reaction, user) =>
          this.onMessageReactionRemove(reaction, user),
        );
      });
    });
  }

  private launchGame(): void {
    this.status = GameStatus.Started;
    this.deck = new Deck(this.users.length);
    this.shuffleRoles();
    this.distributeCards();
    this.cardsToPick = this.users.length;
    this.playingUserIndex = Math.floor(Math.random() * this.users.length);
    this.users.forEach(user => {
      user.sendRoleAndCards();
    });
    this.setGameMessage();
  }

  private shuffleRoles(): void {
    const rolesToDistribute: Array<Role> = GetRolesToAttribute(
      this.users.length,
    );
    this.users.forEach(user => {
      const random = Math.floor(Math.random() * rolesToDistribute.length);
      user.role = rolesToDistribute[random];
      rolesToDistribute.splice(random, 1);
    });
  }

  private distributeCards(): void {
    if (this.deck) {
      const numberOfCards: number = this.deck.cards.length / this.users.length;
      this.users.forEach(user => {
        if (this.deck) {
          user.cards = this.deck.getCardsForUser(numberOfCards);
        }
      });
    }
  }

  private pickUserCard(): void {
    this.lastPickedCard = this.users[this.chosenUserIndex].removeCard();
    let shouldRedistribute = false;

    this.cardsToPick--;
    if (this.cardsToPick == 0) {
      this.roundsToPlay--;
      shouldRedistribute = true;
    }

    if (this.isGameFinished()) {
      this.status = GameStatus.Finished;
      User.deleteTmpFiles();
      this.setGameFinishedMessage();
    } else if (shouldRedistribute) {
      this.cardsToPick = this.users.length;
      this.setShuffleCardsMessage().then(() => {
        setTimeout(() => {
          this.putCardsBackToDeck();
          this.distributeCards();
          this.users.forEach(user => {
            user.sendRoleAndCards();
          });
          this.playingUserIndex = this.chosenUserIndex;
          this.chosenUserIndex = -1;
          this.setGameMessage();
        }, 5000);
      });
    } else {
      this.users.forEach((user, index) => {
        user.sendRoleAndCards(index == this.chosenUserIndex);
      });
      this.playingUserIndex = this.chosenUserIndex;
      this.chosenUserIndex = -1;
      this.setGameMessage(true);
    }
  }

  private onMessageReactionAdd(
    reaction: MessageReaction,
    user: DiscordUser | PartialUser,
  ): void {
    if (this.message && reaction.message == this.message && !user.bot) {
      if (
        this.status == GameStatus.WaitingForPlayers &&
        reaction.emoji.name === '🖐️'
      ) {
        this.addUser(user);
      } else if (
        this.status == GameStatus.WaitingForPlayers &&
        reaction.emoji.name === '💣'
      ) {
        this.launchGame();
      } else if (
        user.id == this.users[this.playingUserIndex].discordUser.id &&
        this.status == GameStatus.Started &&
        User.avatars.includes(reaction.emoji.name)
      ) {
        this.chosenUserIndex = User.avatars.indexOf(reaction.emoji.name);
        this.setGameMessage();
      } else if (
        user.id == this.users[this.playingUserIndex].discordUser.id &&
        this.status == GameStatus.Started &&
        reaction.emoji.name == '⬅️'
      ) {
        this.chosenUserIndex = -1;
        this.setGameMessage();
      } else if (
        user.id == this.users[this.playingUserIndex].discordUser.id &&
        this.status == GameStatus.Started &&
        User.cardChoiceEmoji.includes(reaction.emoji.name)
      ) {
        this.pickUserCard();
      }
    }
  }

  private onMessageReactionRemove(
    reaction: MessageReaction,
    user: DiscordUser | PartialUser,
  ): void {
    if (this.message && reaction.message == this.message && !user.bot) {
      if (
        this.status == GameStatus.WaitingForPlayers &&
        reaction.emoji.name === '🖐️'
      ) {
        this.removeUser(user);
      }
    }
  }

  private addUser(user: DiscordUser | PartialUser): void {
    if (!user.bot) {
      this.users.push(new User(user));
    }
    this.setStartGameMessage();
  }

  private removeUser(user: DiscordUser | PartialUser): void {
    this.users.forEach((item, index) => {
      if (item.discordUser === user) this.users.splice(index, 1);
    });
    this.setStartGameMessage();
  }

  private setStartGameMessage(errorMessage?: string): Promise<Message> {
    const messageEmbed = new MessageEmbed().setTitle('A new game has started');
    if (this.users.length < this._maxPlayers) {
      messageEmbed.setDescription(
        'Waiting for players to join.\nClick on the 🖐️ to join.',
      );
    }
    if (this.users.length > 0) {
      let usersAsString = '';
      this.users.forEach(user => {
        usersAsString += '- <@' + user.discordUser.id + '>\n';
      });
      messageEmbed.addField('List of players', usersAsString);
    }
    if (this.users.length >= this._minPlayers && this.initiator) {
      messageEmbed.addField(
        'To start the game',
        '<@' + this.initiator.id + '> should click on 💣',
      );
    }
    if (errorMessage) {
      messageEmbed.addField('\u200b', '\u200b');
      messageEmbed.addField('Error', errorMessage);
    }
    if (!this.message && this.channel) {
      return Promise.resolve(this.channel.send(messageEmbed));
    } else if (this.message) {
      return this.message.edit(messageEmbed).then((message: Message) => {
        if (this.users.length >= this._minPlayers) {
          message.react('💣');
          // TODO remove 💣 reaction if less than min players
        }
        return Promise.resolve(message);
      });
    } else {
      return Promise.reject('Error');
    }
  }

  private setGameMessage(shouldReplaceMessage = false): void {
    if (this.message) {
      this.message.reactions.removeAll().then(() => {
        const messageEmbed = new MessageEmbed();
        if (this.lastPickedCard) {
          messageEmbed.setTitle('Last picked card ⬆️');
          messageEmbed.attachFiles([Deck.getCardImage(this.lastPickedCard)]);
        }
        messageEmbed.addFields(
          { name: 'Remaining Rounds', value: this.roundsToPlay, inline: true },
          {
            name: 'Remaining Cards to pick',
            value: this.cardsToPick,
            inline: true,
          },
          {
            name: 'Remaining Wires to find',
            value: this.getRemainingGoodWires(),
            inline: true,
          },
          {
            name: 'Player turn',
            value:
              '<@' + this.users[this.playingUserIndex].discordUser.id + '>',
            inline: true,
          },
        );
        let usersToString = '';
        this.users.forEach((user, index) => {
          usersToString +=
            '- ' +
            User.getUserAvatar(index) +
            ' ' +
            user.discordUser.username +
            ' ';
          user.cards.forEach(card => {
            usersToString += '🎴';
          });
          if (index == this.chosenUserIndex) {
            usersToString += ' 🏹';
          }
          usersToString += '\n';
        });
        messageEmbed.addFields(
          { name: '\u200b', value: '\u200b' },
          { name: 'Players in game', value: usersToString },
        );
        messageEmbed.setFooter('Use ⬅️ to cancel player selection');
        if (this.message) {
          if (shouldReplaceMessage) {
            this.message.delete().then(() => {
              if (this.channel) {
                this.channel.send(messageEmbed).then((message: Message) => {
                  this.message = message;
                  if (this.chosenUserIndex >= 0) {
                    this.users[this.chosenUserIndex].cards.forEach(
                      (card, index) => {
                        message.react(User.getCardChoiceEmoji(index));
                      },
                    );
                    message.react('⬅️');
                  } else {
                    this.users.forEach((user, index) => {
                      if (
                        user.cards.length > 0 &&
                        index != this.playingUserIndex
                      ) {
                        message.react(User.getUserAvatar(index));
                      }
                    });
                  }
                });
              }
            });
          } else {
            this.message.edit(messageEmbed).then((message: Message) => {
              if (this.chosenUserIndex >= 0) {
                this.users[this.chosenUserIndex].cards.forEach(
                  (card, index) => {
                    message.react(User.getCardChoiceEmoji(index));
                  },
                );
                message.react('⬅️');
              } else {
                this.users.forEach((user, index) => {
                  if (user.cards.length > 0 && index != this.playingUserIndex) {
                    message.react(User.getUserAvatar(index));
                  }
                });
              }
            });
          }
        }
      });
    }
  }

  private setShuffleCardsMessage(): Promise<Message> {
    if (this.message) {
      return this.message.delete().then(() => {
        const messageEmbed = new MessageEmbed();
        if (this.lastPickedCard) {
          messageEmbed.setTitle('Last picked card ⬆️');
          messageEmbed.attachFiles([Deck.getCardImage(this.lastPickedCard)]);
        }
        messageEmbed.setDescription(
          'The round has ended, \ncards are getting shuffled and redistributed',
        );
        messageEmbed.addFields(
          { name: 'Remaining Rounds', value: this.roundsToPlay, inline: true },
          {
            name: 'Remaining Cards to pick',
            value: this.cardsToPick,
            inline: true,
          },
          {
            name: 'Remaining Wires to find',
            value: this.getRemainingGoodWires(),
            inline: true,
          },
        );
        if (this.channel) {
          return this.channel.send(messageEmbed).then((message: Message) => {
            this.message = message;
            return Promise.resolve(message);
          });
        } else {
          return Promise.reject();
        }
      });
    } else {
      return Promise.reject();
    }
  }

  private setGameFinishedMessage(): void {
    if (this.message) {
      this.message.delete().then(() => {
        const messageEmbed = new MessageEmbed();
        if (this.lastPickedCard) {
          messageEmbed
            .setTitle('Last picked card ⬆️')
            .attachFiles([Deck.getCardImage(this.lastPickedCard)]);
        }
        messageEmbed.setDescription('The game is finished');
        if (this.getRemainingGoodWires() == 0) {
          messageEmbed
            .setColor('#0b7489')
            .addField(
              'The winning team is',
              Role.Sherlock + '\nThe managed to find every bomb.',
            );
        } else {
          messageEmbed.setColor('#ac203a');
          messageEmbed.addField(
            'The winning team is',
            Role.Moriarty +
              (this.lastPickedCard == Card.Bomb
                ? '\nThey managed to find the bomb.'
                : '\nThey managed to avoid finding every bomb.'),
          );
        }
        if (this.channel) {
          this.channel.send(messageEmbed);
        }
      });
    }
  }

  private putCardsBackToDeck(): void {
    this.users.forEach(user => {
      user.cards.forEach(card => {
        if (this.deck) {
          this.deck.cards.push(card);
        }
      });
      user.cards = [];
    });
  }

  private getRemainingGoodWires(): number {
    let remainingGoodWires = 0;
    this.users.forEach(user => {
      user.cards.forEach(card => {
        remainingGoodWires += card == Card.GoodWire ? 1 : 0;
      });
    });
    return remainingGoodWires;
  }

  private isGameFinished(): boolean {
    return (
      this.lastPickedCard == Card.Bomb ||
      this.getRemainingGoodWires() == 0 ||
      (this.roundsToPlay <= 0 && this.cardsToPick <= 0)
    );
  }
}
