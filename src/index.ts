require('dotenv').config();
import { Client } from 'discord.js';
import { Bot } from './bot';

const bot = new Bot(new Client());

bot
  .listen()
  .then(() => {
    console.log('Logged in!');
  })
  .catch(error => {
    console.log('Oh no! ', error);
  });
